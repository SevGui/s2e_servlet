package com.greta.s2e_servlet.globals;

public class Constants {
	
	/**
	 * ATTRIBUTES
	 */
	//envoi vers la servlet
	public static final String PAGE_HISTORIQUE 			= "/story";
	public static final String PAGE_DEMANDE 			= "/new";
	public static final String PAGE_DECONNEXION 		= "/deconnexion";
	
	//Texte boutons
	public static final String MENU_HISTORIQUE 			= "Historique des demandes";
	public static final String MENU_DEMANDE 			= "Nouvelle demande";
	public static final String MENU_DECONNEXION 		= "Déconnexion";
	
	//attributs pour les setAttribute
	public static final String DEMANDE_ATTR				= "demande";
	public static final String BUDGET_INT_ATTR			= "budgetint";
	public static final String BUDGET_RST_ATTR			= "budgetrest";
	public static final String ARTICLE_ATTR				= "article";
	public static final String ERROR_DMD_ATTR			= "erreur";
	
	/* Paramètres du cookie qui contiendra l'id de l'utilisateur connecté */
	public static final String COOKIE_S2E 				= "s2e_user";
	public static final String COOKIE_S2E_PATH 			= "/";
	
	/* Paramètres du session qui contiendra l'utilisateur connecté */
	public static final String SESSION_S2E 				= "s2e_user";
	
	/* Droits */
	public static final String DROIT_ETABLISSEMENT		= "ET";
	
	//envoi vers les JSP
	public static final String STORY_PAGE				= "/story.jsp";
	public static final String NEW_PAGE					= "/WEB-INF/new.jsp";
	public static final String AJAX_PAGE				= "/WEB-INF/ajax.jsp";
//	public static final String REDIRECTION_LOGIN 		= "http://10.120.112.120:8080/s2e_accueil_struts/";
	public static final String REDIRECTION_LOGIN 		= "http://localhost:8080/s2e_accueil_struts/";
	
	//Paramètres dans les JSP pour les Post
	public static final String ARTICLE_PARAM			= "article";
	public static final String ARTICLEEQUI_PARAM		= "articleequi";
	public static final String QUANT_PARAM				= "quantite";
	
//	// Différents messages
	public static final String SUCCESS_MESSAGE			= "Votre demande a été prise en compte";
	public static final String FAILURE_MESSAGE			= "Votre demande n'a pas été prise en compte, je vous invite à ré-essayer";
	public static final String ARTICLE_VALIDATION_ERROR = "Veuillez sélectionner un article dans la liste et mettre une quantité >0";
	
	/* Database connection parameters */
	public static final String URL 						= "jdbc:mysql://127.0.0.1:3306/s2e_test";
	public static final String SQLUSER 					= "severine";
	public static final String SQLPASSWD 				= "azerty";
	public static final String ORACLEDRIVER 			= "com.mysql.jdbc.Driver";
	public static final String BEGINPAGE				= "/WEB-INF/jdbczero.jsp";
	
	/* Constante pour les requêtes */
	public static final String REFU_ETAT				= "REFU";
	public static final String VALI_ETAT				= "ATVAL";
	public static final int DELAI_DEMANDE				= 1;
	
	/* Nom de la table demandes et des colonnes de la base de données */
	public static final String DEMANDE					= "demandes";
	public static final String DMD_ID					= "dmdId";
	public static final String DMD_QTE					= "dmdQuant";
	public static final String DMD_DELAI				= "dmdDelai";
	public static final String DMD_DATE					= "dmdDate";
	public static final String DMD_ID_USER				= "dmdIdUtil";
	public static final String DMD_ID_ART				= "dmdIdArt";
	public static final String DMD_ID_ETAT				= "dmdIdEtat";
	
	/* Nom de la table etats et des colonnes de la base de données */
	public static final String ETATS					= "etats";
	public static final String ETT_ID					= "etatId";
	public static final String ETT_LIB					= "etatLibelle";
	
	/* Nom de la table articles et des colonnes de la base de données */
	public static final String ARTICLES					= "articles";
	public static final String ART_ID					= "artId";
	public static final String ART_NOM					= "artNom";
	public static final String ART_MARQUE				= "artMarque";
	public static final String ART_MODELE				= "artModele";
	public static final String ART_EQUI					= "artEqui";
	public static final String ART_PRIXM				= "artPrixMoyen";
	public static final String ART_QTETA				= "artQuanTA";
	
	/* Nom de la table articles et des colonnes de la base de données */
	public static final String STOCK					= "stock";
	public static final String STK_ID					= "stockId";
	public static final String STK_QUANT				= "stockQuant";
	public static final String STK_QUANTMIN				= "stockQuantMin";
	public static final String STK_QUANTMAX				= "stockQuantMax";
	public static final String STK_ID_ART				= "stockIdArt";
	
	/* Nom de la table utilisateurs et des colonnes de la base de données */
	public static final String USERS					= "utilisateurs";
	public static final String USR_ID					= "utlId";
	public static final String USR_NOM					= "utlNom";
	public static final String USR_PRENOM				= "utlPrenom";
	public static final String USR_LOGIN				= "utlLogin";
	public static final String USR_PASSWORD				= "utlMdp";
	public static final String USR_ID_ROLE				= "utlIdRole";
	public static final String USR_ID_ETB				= "utlIdEtab";
	
	/* Nom de la table etablissement et des colonnes de la base de données */
	public static final String ETAB						= "etablissement";
	public static final String ETB_ID					= "etabId";
	public static final String ETB_NOM					= "etabNom";
	public static final String ETB_ADR					= "etabAdres";
	public static final String ETB_CP					= "etabCP";
	public static final String ETB_VILLE				= "etabVille";
	public static final String ETB_BUDG					= "etabBudget";
	
	public Constants() {
		// TODO Auto-generated constructor stub
	}

}
