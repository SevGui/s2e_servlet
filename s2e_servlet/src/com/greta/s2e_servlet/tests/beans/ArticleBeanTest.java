package com.greta.s2e_servlet.tests.beans;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized;

import com.greta.s2e_servlet.beans.ArticleBean;

@RunWith(Parameterized.class)
public class ArticleBeanTest {
	
	private ArticleBean article;
	private ArticleBean article1;
	private ArticleBean article2;
	
	private int id;
	private int expectedId;
	private String nom;
	private String expectedNom;
	private String marque;
	private String expectedMarque;
	private String modele;
	private String expectedModele;
	private int equi;
	private int expectedEqui;
	private Float prix;
	private Float expectedPrix;
	

	@Parameters
	public static Collection<Object[]> dataName(){
		return Arrays.asList(new Object[][]{
			{1,1,"Vis","Vis","Facom","Facom","OS110","OS110",1,1,15.6f,15.6f},
			{2,2,"Boulon","Boulon","Instrument","Instrument","TV_540","TV_540",3,3,0.6f,0.6f}
		});
	}


	/**
	 * @param id
	 * @param expectedId
	 * @param nom
	 * @param expectedNom
	 * @param marque
	 * @param expectedMarque
	 * @param modele
	 * @param expectedModele
	 * @param equi
	 * @param expectedEqui
	 * @param prix
	 * @param expectedPrix
	 */
	public ArticleBeanTest(int id, int expectedId, String nom, String expectedNom, String marque, String expectedMarque,
			String modele, String expectedModele, int equi, int expectedEqui, Float prix, Float expectedPrix) {
		super();
		this.id = id;
		this.expectedId = expectedId;
		this.nom = nom;
		this.expectedNom = expectedNom;
		this.marque = marque;
		this.expectedMarque = expectedMarque;
		this.modele = modele;
		this.expectedModele = expectedModele;
		this.equi = equi;
		this.expectedEqui = expectedEqui;
		this.prix = prix;
		this.expectedPrix = expectedPrix;
	}


	@Before
	public void setUp() throws Exception {
		article = new ArticleBean(id, nom, marque, modele, equi);
		article1 = new ArticleBean(id);
		article2 = new ArticleBean(nom, prix);
	}
	
	@After
	public void tearDown(){
		article = null;
		article1 = null;
		article2 = null;
	}

	@Test
	public final void testGetId_Article() {
		assertTrue("when constructor with arguments is called, id attribute is set",
				expectedId == article.getId_Article());
	}
	
	@Test
	public final void testGetId_Article1() {
		assertTrue("when constructor with arguments is called, id attribute is set",
				expectedId == article1.getId_Article());
	}

	@Test
	public final void testGetNom() {
		assertEquals("when constructor with arguments is called, nom attribute is set",
				expectedNom, article.getNom());
	}
	
	@Test
	public final void testGetNom2() {
		assertEquals("when constructor with arguments is called, nom attribute is set",
				expectedNom, article2.getNom());
	}

	@Test
	public final void testGetMarque() {
		assertEquals("when constructor with arguments is called, marque attribute is set",
				expectedMarque, article.getMarque());
	}

	@Test
	public final void testGetModele() {
		assertEquals("when constructor with arguments is called, modele attribute is set",
				expectedModele, article.getModele());
	}

	@Test
	public final void testGetEqui() {
		assertTrue("when constructor with arguments is called, equi attribute is set",
				expectedEqui == article.getEqui());
	}

	@Test
	public final void testGetPrix2() {
		assertEquals("when constructor with arguments is called, equi attribute is set",
				expectedPrix, article2.getPrix(), 0f);
	}
}
