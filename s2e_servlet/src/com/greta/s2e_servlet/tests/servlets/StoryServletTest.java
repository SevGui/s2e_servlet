package com.greta.s2e_servlet.tests.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StoryServletTest {
	
	@Mock
	HttpServletRequest request;
	@Mock
	HttpServletResponse response;
	@Mock
	RequestDispatcher dispatcher;
	@Mock
	HttpSession session;

	@Before
	public void setUp() throws Exception {
		when(request.getSession()).thenReturn(session);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testDoGet() {
		fail("Not yet implemented"); // TODO
	}

}
