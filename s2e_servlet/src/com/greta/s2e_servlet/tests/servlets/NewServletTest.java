package com.greta.s2e_servlet.tests.servlets;

import static com.greta.s2e_servlet.globals.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.greta.s2e_servlet.beans.ArticleBean;
import com.greta.s2e_servlet.beans.UserBean;
import com.greta.s2e_servlet.dao.ArticleDao;
import com.greta.s2e_servlet.dao.BudgetDao;
import com.greta.s2e_servlet.servlets.NewServlet;

@RunWith(MockitoJUnitRunner.class)
public class NewServletTest {

	@Mock
	HttpServletRequest request;
	@Mock
	HttpServletResponse response;
	@Mock
	RequestDispatcher dispatcher;
	@Mock
	HttpSession session;

	private static final Logger LOGGER = Logger.getLogger(NewServletTest.class);
	private String page = NEW_PAGE;
	private UserBean user;
	private BudgetDao etabDao;
	private ArticleDao articleDao;
	private NewServlet newServlet;
	private Float budgetInt;
	private Float budgetRest;
	private Float montantTot;
	private ArrayList<ArticleBean> articleList;

	@Before
	public void setUp() throws Exception {
		newServlet = new NewServlet();
		when(request.getRequestDispatcher(page)).thenReturn(dispatcher);
		when(request.getSession()).thenReturn(session);
		user = new UserBean(2, "Robic", "Virginie", "rvirginie", "azerty", "ET", 2);
		when(session.getAttribute(SESSION_S2E)).thenReturn(user);
		// when(request.getContextPath()).thenReturn("");
		etabDao = new BudgetDao();
		articleDao = new ArticleDao();
		budgetInt = etabDao.getBudget(user);
		LOGGER.info("budget " + budgetInt);
		montantTot = etabDao.getMontantTotal(user);
		LOGGER.info("montant " + montantTot);
		budgetRest = budgetInt - montantTot;
		articleList = new ArrayList<ArticleBean>();
		articleList = articleDao.articleList();

	}

	@After
	public void tearDown() throws Exception {
		user = null;
		etabDao = null;
		budgetInt = null;
		montantTot = null;
		budgetRest = null;
	}

	@Test
	public final void testdoCommon() throws ServletException, IOException {
		newServlet.doCommon(NEW_PAGE, session, request, response);
		ArgumentCaptor<String> dispatcherArg = ArgumentCaptor.forClass(String.class);
		verify(request).getRequestDispatcher(dispatcherArg.capture());
		assertEquals(NEW_PAGE, dispatcherArg.getValue());
	}

	@Test
	public final void testDoGet() throws ServletException, IOException {
		// appel de la méthode à tester
		newServlet.doGet(request, response);

		// récupère tous les arguments de type float pendant l'execution du
		// doGet
		ArgumentCaptor<Float> budgetRestArg = ArgumentCaptor.forClass(Float.class);
		// 1er temps : vérifie qu'il y a bien une request passée pour le
		// setattribute
		// 2ème temps renvoie la valeur passée en argument du setattribute
		// en fonction des arguments renvoyés avec argumentCaptor
		verify(request).setAttribute(eq(BUDGET_RST_ATTR), budgetRestArg.capture());
		// Vérifie l'égalité entre le float budgetRest et la valeur récupérée
		assertEquals(budgetRest, budgetRestArg.getValue(), 0f);


		@SuppressWarnings("unchecked")
		Class<ArrayList<ArticleBean>> listClass = (Class<ArrayList<ArticleBean>>)(Class)ArrayList.class;
		ArgumentCaptor<ArrayList<ArticleBean>> articleListArg = ArgumentCaptor.forClass(listClass);
		verify(request).setAttribute(eq(ARTICLE_ATTR), articleListArg.capture());
		assertEquals(articleList.get(0).getNom(), articleListArg.getValue().get(0).getNom());
	}

	@Test
	public final void testDoPost() {
		fail("Not yet implemented"); // TODO
	}

}
