package com.greta.s2e_servlet.servlets;

import java.io.IOException;

import static com.greta.s2e_servlet.globals.Constants.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class DeconnexionServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(DeconnexionServlet.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Destruction de la session
		LOGGER.info("Déconnexion : destruction de la session");
		HttpSession session = request.getSession();
		session.invalidate();
		//redirection sur l'index -> application du filtre -> redirection sur la page de connexion
		response.sendRedirect(request.getContextPath() + STORY_PAGE);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
