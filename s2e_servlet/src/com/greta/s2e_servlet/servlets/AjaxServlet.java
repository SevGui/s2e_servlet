package com.greta.s2e_servlet.servlets;

import static com.greta.s2e_servlet.globals.Constants.*;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.greta.s2e_servlet.beans.ArticleBean;
import com.greta.s2e_servlet.beans.UserBean;
import com.greta.s2e_servlet.services.DemandeServices;

public class AjaxServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(AjaxServlet.class);
	
	private UserBean user = null;
	private DemandeServices demandeService;
	private ArrayList<ArticleBean> articleList;
	
	/**
	 * 
	 */
	public AjaxServlet() {
		super();
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Ajax");
		HttpSession session = request.getSession();
		demandeService = new DemandeServices();
		user = (UserBean) session.getAttribute(SESSION_S2E);
		articleList = new ArrayList<ArticleBean>();
		
		//Vérification de la demande en fonction du stock
		articleList = demandeService.verifyStockDemande(request);
		
		request.setAttribute(ARTICLEEQUI_PARAM, articleList);

		doCommon(AJAX_PAGE, session,request,response);
	}
	
	private void doCommon(String page, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher(page).forward(request, response); 
	}
}
