package com.greta.s2e_servlet.servlets;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.tomcat.jni.User;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.greta.s2e_servlet.beans.ArticleBean;
import com.greta.s2e_servlet.beans.DemandeBean;
import com.greta.s2e_servlet.beans.EtatBean;
import com.greta.s2e_servlet.beans.UserBean;
import com.greta.s2e_servlet.dao.ArticleDao;
import com.greta.s2e_servlet.dao.BudgetDao;
import com.greta.s2e_servlet.dao.RequestDao;
import com.greta.s2e_servlet.services.DemandeServices;

public class NewServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(NewServlet.class);
	private BudgetDao etabDao;
	private ArticleDao articleDao;
	private ArrayList<ArticleBean> articleList;
	private UserBean user = null;
	private DemandeServices demandeService;
	
	private Float budgetInt;
	private Float budgetRest;
	private Float montantTot;
	
	
	/**
	 * 
	 */
	public NewServlet() {
		super();
	}

	public void doGet( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		etabDao = new BudgetDao();
		articleDao = new ArticleDao();
		articleList = new ArrayList<ArticleBean>();
		user = (UserBean) session.getAttribute(SESSION_S2E);
		
		//Récupération de la liste de tous les articles
		articleList = articleDao.articleList();
		
		//Récupération du budget
		budgetInt = etabDao.getBudget(user);
		LOGGER.info("budget " + budgetInt);
		
		//Récupération de la somme des demandes
		montantTot = etabDao.getMontantTotal(user);
		LOGGER.info("somme demandes " + montantTot);
		
		//Détermination du budget restant
		budgetRest = budgetInt-montantTot;
		
		request.setAttribute(BUDGET_RST_ATTR, budgetRest);
		request.setAttribute(ARTICLE_ATTR, articleList);
		doCommon(NEW_PAGE, session,request,response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		demandeService = new DemandeServices();
		user = (UserBean) session.getAttribute(SESSION_S2E);
		
		//Création d'une demande
		demandeService.createDemande(request, user);
		LOGGER.info("doPost Demande pas erreur formulaire " + demandeService.getErrors().isEmpty());
		//si il n'y a pas d'erreur
		if (demandeService.getErrors().isEmpty()) {
			//redirection vers la page story
			response.sendRedirect(request.getContextPath() + PAGE_HISTORIQUE);
		} else {
			//retour vers le formulaire avec les erreurs
			request.setAttribute(ERROR_DMD_ATTR, demandeService);
			doGet(request, response);
		}
		
	}

	public void doCommon(String page, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher(page).forward(request, response); 
	}
}
