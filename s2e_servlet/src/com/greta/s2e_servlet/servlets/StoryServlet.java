package com.greta.s2e_servlet.servlets;

import static com.greta.s2e_servlet.globals.Constants.*;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.greta.s2e_servlet.beans.DemandeBean;
import com.greta.s2e_servlet.beans.UserBean;
import com.greta.s2e_servlet.dao.BudgetDao;
import com.greta.s2e_servlet.dao.UserDao;
import com.greta.s2e_servlet.services.DemandeServices;

public class StoryServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(StoryServlet.class);
	private DemandeServices demandeService;
	private BudgetDao etabDao;
	private ArrayList<DemandeBean> demandeList;
	private UserBean user = null;
	
	private Float budgetInt;
	private Float budgetRest;
	private Float montantTot;

	
	/**
	 * 
	 */
	public StoryServlet() {
		super();
	}

	public void doGet( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		demandeService = new DemandeServices();
		etabDao = new BudgetDao();
		demandeList = new ArrayList<DemandeBean>();
		user = (UserBean) session.getAttribute(SESSION_S2E);
		
		//Récupération de la liste des demandes
		demandeList =  demandeService.requestList(user);
		LOGGER.info("list des demandes " + demandeList);
		
		//Récupération du budget
		budgetInt = etabDao.getBudget(user);
		LOGGER.info("budget " + budgetInt);
		
		//Récupération de la somme des demandes
		montantTot = etabDao.getMontantTotal(user);
		LOGGER.info("somme demandes " + montantTot);
		
		//Détermination du budget restant
		budgetRest = budgetInt-montantTot;
		
		request.setAttribute(DEMANDE_ATTR, demandeList);
		request.setAttribute(BUDGET_INT_ATTR, budgetInt);
		request.setAttribute(BUDGET_RST_ATTR, budgetRest);
		doCommon(STORY_PAGE, session,request,response);
	}

	private void doCommon(String page, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher(page).forward(request, response); 
	}
}
