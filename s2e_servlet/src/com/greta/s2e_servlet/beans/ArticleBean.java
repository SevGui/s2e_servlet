package com.greta.s2e_servlet.beans;

public class ArticleBean {

	private int id_Article;
	private String nom;
	private String marque;
	private String modele;
	private int equi;
	private Float prix;
	private int quantTotAch;
	private StockBean stock;

	
	/**
	 * @param id_Article
	 * @param nom
	 * @param marque
	 * @param modele
	 * @param equi
	 */
	public ArticleBean(int id_Article, String nom, String marque, String modele, int equi) {
		super();
		this.id_Article = id_Article;
		this.nom = nom;
		this.marque = marque;
		this.modele = modele;
		this.equi = equi;
	}

	/**
	 * @param nom
	 * @param prix
	 */
	public ArticleBean(String nom, Float prix) {
		super();
		this.nom = nom;
		this.prix = prix;
	}



	/**
	 * @param id_Article
	 */
	public ArticleBean(int id_Article) {
		super();
		this.id_Article = id_Article;
	}



	/**
	 * 
	 */
	public ArticleBean() {
		super();
	}

	public int getId_Article() {
		return id_Article;
	}

	public void setId_Article(int id_Article) {
		this.id_Article = id_Article;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}


	public void setModele(String modele) {
		this.modele = modele;
	}


	public int getEqui() {
		return equi;
	}

	public void setEqui(int equi) {
		this.equi = equi;
	}

	public Float getPrix() {
		return prix;
	}

	public void setPrix(Float prix) {
		this.prix = prix;
	}

	public int getQuantTotAch() {
		return quantTotAch;
	}

	public void setQuantTotAch(int quantTotAch) {
		this.quantTotAch = quantTotAch;
	}



	/**
	 * @return the stock
	 */
	public StockBean getStock() {
		return stock;
	}



	/**
	 * @param stock the stock to set
	 */
	public void setStock(StockBean stock) {
		this.stock = stock;
	}
	
	
}
