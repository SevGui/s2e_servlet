package com.greta.s2e_servlet.beans;

public class UserBean {

	private int id_User;
	private String nom;
	private String prenom;
	private String login;
	private String mdp;
	private String id_Role;
	private int id_Etab;
	
	
	/**
	 * @param id_User
	 * @param nom
	 * @param prenom
	 * @param login
	 * @param mdp
	 * @param id_Role
	 * @param id_Etab
	 */
	public UserBean(int id_User, String nom, String prenom, String login, String mdp, String id_Role, int id_Etab) {
		super();
		this.id_User = id_User;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.mdp = mdp;
		this.id_Role = id_Role;
		this.id_Etab = id_Etab;
	}
	
	/**
	 * @param id_User
	 */
	public UserBean(int id_User) {
		super();
		this.id_User = id_User;
	}

	/**
	 * @return the id_User
	 */
	public int getId_User() {
		return id_User;
	}
	/**
	 * @param id_User the id_User to set
	 */
	public void setId_User(int id_User) {
		this.id_User = id_User;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the mdp
	 */
	public String getMdp() {
		return mdp;
	}
	/**
	 * @param mdp the mdp to set
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	/**
	 * @return the id_Role
	 */
	public String getId_Role() {
		return id_Role;
	}
	/**
	 * @param id_Role the id_Role to set
	 */
	public void setId_Role(String id_Role) {
		this.id_Role = id_Role;
	}
	/**
	 * @return the id_Etab
	 */
	public int getId_Etab() {
		return id_Etab;
	}
	/**
	 * @param id_Etab the id_Etab to set
	 */
	public void setId_Etab(int id_Etab) {
		this.id_Etab = id_Etab;
	}
	
}
