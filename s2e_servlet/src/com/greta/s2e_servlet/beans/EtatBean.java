package com.greta.s2e_servlet.beans;

public class EtatBean {

	private String id_Etat;
	private String libelle;
	
	/**
	 * @param id_Etat
	 * @param libelle
	 */
	public EtatBean(String id_Etat, String libelle) {
		super();
		this.id_Etat = id_Etat;
		this.libelle = libelle;
	}

	
	/**
	 * @param libelle
	 */
	public EtatBean(String libelle) {
		super();
		this.libelle = libelle;
	}


	/**
	 * 
	 */
	public EtatBean() {
		super();
	}


	public String getId_Etat() {
		return id_Etat;
	}

	public void setId_Etat(String id_Etat) {
		this.id_Etat = id_Etat;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
