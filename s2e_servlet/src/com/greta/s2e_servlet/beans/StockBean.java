package com.greta.s2e_servlet.beans;

public class StockBean {

	private int id_Stock;
	private int quantStock;
	private int quantMin;
	private int quantMax;
	private ArticleBean article;
	
	
	/**
	 * @param quantStock
	 */
	public StockBean(int quantStock) {
		super();
		this.quantStock = quantStock;
	}
	/**
	 * @return the id_Stock
	 */
	public int getId_Stock() {
		return id_Stock;
	}
	/**
	 * @param id_Stock the id_Stock to set
	 */
	public void setId_Stock(int id_Stock) {
		this.id_Stock = id_Stock;
	}
	/**
	 * @return the quantStock
	 */
	public int getQuantStock() {
		return quantStock;
	}
	/**
	 * @param quantStock the quantStock to set
	 */
	public void setQuantStock(int quantStock) {
		this.quantStock = quantStock;
	}
	/**
	 * @return the quantMin
	 */
	public int getQuantMin() {
		return quantMin;
	}
	/**
	 * @param quantMin the quantMin to set
	 */
	public void setQuantMin(int quantMin) {
		this.quantMin = quantMin;
	}
	/**
	 * @return the quantMax
	 */
	public int getQuantMax() {
		return quantMax;
	}
	/**
	 * @param quantMax the quantMax to set
	 */
	public void setQuantMax(int quantMax) {
		this.quantMax = quantMax;
	}
	/**
	 * @return the article
	 */
	public ArticleBean getArticle() {
		return article;
	}
	/**
	 * @param article the article to set
	 */
	public void setArticle(ArticleBean article) {
		this.article = article;
	}

}
