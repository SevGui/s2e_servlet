package com.greta.s2e_servlet.beans;

import org.joda.time.LocalDate;

public class DemandeBean {

	private int id_Demande;
	private int quantite;
	private int delai;
	private LocalDate date;
	private UserBean user;
	private ArticleBean article;
	private EtatBean etat;

	

	/**
	 * @param quantite
	 * @param delai
	 * @param date
	 * @param user
	 * @param article
	 * @param etat
	 */
	public DemandeBean(int quantite, int delai, LocalDate date, UserBean user, ArticleBean article, EtatBean etat) {
		super();
		this.quantite = quantite;
		this.delai = delai;
		this.date = date;
		this.user = user;
		this.article = article;
		this.etat = etat;
	}


	/**
	 * @param quantite
	 * @param date
	 * @param article
	 * @param etat
	 */
	public DemandeBean(int quantite, LocalDate date, ArticleBean article, EtatBean etat) {
		super();
		this.quantite = quantite;
		this.date = date;
		this.article = article;
		this.etat = etat;
	}


	/**
	 * 
	 */
	public DemandeBean() {
		super();
	}


	public int getId_Demande() {
		return id_Demande;
	}


	public void setId_Demande(int id_Demande) {
		this.id_Demande = id_Demande;
	}


	public int getQuantite() {
		return quantite;
	}


	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}


	public int getDelai() {
		return delai;
	}


	public void setDelai(int delai) {
		this.delai = delai;
	}


	public LocalDate getDate() {
		return date;
	}


	public void setDate(LocalDate date) {
		this.date = date;
	}


	public UserBean getUser() {
		return user;
	}


	public void setUser(UserBean user) {
		this.user = user;
	}


	public ArticleBean getArticle() {
		return article;
	}


	public void setArticle(ArticleBean article) {
		this.article = article;
	}


	public EtatBean getEtat() {
		return etat;
	}


	public void setEtat(EtatBean etat) {
		this.etat = etat;
	}
	
	
	
}
