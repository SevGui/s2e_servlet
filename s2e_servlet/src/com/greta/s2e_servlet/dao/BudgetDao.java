package com.greta.s2e_servlet.dao;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.greta.s2e_servlet.beans.UserBean;

public class BudgetDao extends Dao{

	private static final Logger LOGGER = Logger.getLogger(BudgetDao.class);
	
	/**
	 * Récupération du budget en fonction de l'établissement
	 */
	public Float getBudget(UserBean user){
		LOGGER.info("ok getBudget");
		PreparedStatement preparedStatement = null;
		
		ResultSet resultat = null;
		Float result = 0f;
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();

		try {
			// Préparation de la requête
			if (null != connection) {
				preparedStatement = connection.prepareStatement("SELECT " + ETB_BUDG + " FROM " + ETAB 
						+ " WHERE " + ETB_ID + " = ?");
				preparedStatement.setInt(1, user.getId_Etab());
			} else {
				LOGGER.fatal("Connexion null");
			}
			
			// Execution de la requête
			try {
				resultat = preparedStatement.executeQuery();
			} catch (SQLException exception) {
				LOGGER.fatal("Erreur requête : " + exception);
			}
			
			LOGGER.info("requete " + resultat);
			
			//Récupération de la valeur
			while (resultat.next()) {
				result = Float.valueOf(resultat.getString(1));
				LOGGER.info("requete en forme " + result.toString());
			}
			
		} catch (SQLException exception) {
			LOGGER.fatal("query error : " + exception);
		} finally {
			//fermeture de la connexion
			sqlDeconnection();
		}
		return result;
	};
	
	/**
	 * Récupération de la somme des demandes pour l'année en fonction de l'établissement
	 * en excluant les demandes refusées
	 */
	public Float getMontantTotal(UserBean user){
		LOGGER.info("ok getMontantTotal");
		PreparedStatement preparedStatement = null;
		
		ResultSet resultat = null;
		Float result = 0f;
		
		//récupération de la date du jour
		LocalDate dateJ= LocalDate.now();
		//format que l'on veut pour la date
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy");
		//Mise au format avec le premier jour de l'année et le dernier
		String janvierDate = fmt.print(dateJ) + "-01-01";
		String decembreDate = fmt.print(dateJ) + "-12-31";
		LOGGER.info("date " + janvierDate + " " + decembreDate);
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();
		
		try {
			// Préparation de la requête
			if (null != connection) {
				preparedStatement = connection.prepareStatement("SELECT SUM("+ DMD_QTE +"*"+ ART_PRIXM +") FROM " + DEMANDE
						+ " INNER JOIN " + ETATS + " ON " + DMD_ID_ETAT + " = " + ETT_ID 
						+ " INNER JOIN " + ARTICLES + " ON " + DMD_ID_ART + " = "+ ART_ID
						+ " INNER JOIN " + USERS + " ON " + DMD_ID_USER + " = "+ USR_ID
						+ " WHERE " + USR_ID_ETB + " = ?"
						+ " AND "+ ETT_ID +" <> ?"
						+ " AND "+ DMD_DATE +" BETWEEN ? AND ?");
				preparedStatement.setInt(1, user.getId_Etab());
				preparedStatement.setString(2, REFU_ETAT);
				preparedStatement.setString(3, janvierDate);
				preparedStatement.setString(4, decembreDate);
			} else {
				LOGGER.fatal("Connexion null");
			}
			
			// Execution de la requête
			try {
				resultat = preparedStatement.executeQuery();
			} catch (SQLException exception) {
				LOGGER.fatal("Erreur requête : " + exception);
			}
			
			LOGGER.info("requete " + resultat);
			
			//récupération de la valeur
			while (resultat.next()) {
				result = Float.valueOf(resultat.getString(1));
				LOGGER.info("requete en forme " + result.toString());
			}
			
		} catch (SQLException exception) {
			LOGGER.fatal("query error : " + exception);
		} finally {
			sqlDeconnection();
		}
		return result;
	}

}
