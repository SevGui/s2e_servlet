package com.greta.s2e_servlet.dao;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class Dao {

	private static final Logger LOGGER = Logger.getLogger(Dao.class);
	protected Connection myConnection = null;
	protected Statement statement = null;
	protected Connection connection = null;

	protected void sqlLoadDriver() {
		/* Loading driver JDBC Oracle */
		try {
			Class.forName(ORACLEDRIVER);
			LOGGER.info("ok loading driver");
		} catch (ClassNotFoundException exception) {
			LOGGER.fatal("could not load driver! " + exception);
		}
	}

	protected void sqlConnection1() {
		// Récupération des informations pour la connection à la base
		try {
			myConnection = DriverManager.getConnection(URL, SQLUSER, SQLPASSWD);
			LOGGER.info("successfull connection");
			if (null != myConnection) {
				statement = myConnection.createStatement();
			} else {
				LOGGER.fatal("connection returns null when creating statement.");
			}
		} catch (SQLException exception) {
			LOGGER.fatal("caught connection error: " + exception);
		}
	}
	
	/**
	 * sqlConnection : connexion à  la BDD
	 * constantes définies dans la classe com.greta.s2e_servlet.globals.Constantes
	 */
	protected void sqlConnection() {
		//connexion à  la BDD
		try {
			connection = DriverManager.getConnection(URL, SQLUSER, SQLPASSWD);
			LOGGER.info("Connexion réussie");
		} catch (SQLException exception) {
			LOGGER.fatal("caught connection error: " + exception);
		}
	}

	protected void sqlDeconnection() {
		// Fermeture de la connection
		if (null != myConnection)
			try {
				LOGGER.info("closing connection");
				myConnection.close();
				LOGGER.info("connection closed");
			} catch (SQLException ignore) {
				LOGGER.debug("ignored error when closing: " + ignore);
			}
	}
}
