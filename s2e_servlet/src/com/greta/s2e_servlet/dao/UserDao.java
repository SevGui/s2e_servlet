package com.greta.s2e_servlet.dao;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.greta.s2e_servlet.beans.UserBean;

public class UserDao extends Dao {

	private static final Logger LOGGER = Logger.getLogger(UserDao.class);

	/**
	 * getById : récupére les données d'un utilisateur en base de données à 
	 * partir de son id
	 * 
	 * @param int id
	 * @return Utilisateur : objet entity contenant les données constantes
	 *         définies dans la classe com.greta.s2e_servlet.globals.Constants
	 */
	public UserBean getById(int id) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		UserBean utilisateur = null;

		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();

		try {
			// Préparation de la requête
			// le mot de passe est encrypté dans la requête pour être
			// comparé avec celui, encrypté, de la BDD
			if (null != connection) {
				preparedStatement = connection.prepareStatement("SELECT * FROM " + USERS 
						+ " WHERE " + USR_ID + "=?;");
				preparedStatement.setInt(1, id);
			} else {
				LOGGER.fatal("Connexion null");
			}

			// Execution de la requête
			try {
				resultSet = preparedStatement.executeQuery();
			} catch (SQLException exception) {
				LOGGER.fatal("Erreur requête : " + exception);
			}

			// Mappage du ResultSet en Utilisateur
			if (resultSet.next()) {
				utilisateur = map(resultSet);
			}
			
		} catch (SQLException exception) {
			LOGGER.fatal("Erreur SQL : " + exception);
		} finally{
			//fermeture de la connexion
			sqlDeconnection();
		}
		return utilisateur;
	}

	/**
	 * map : parse les données d'un ResulSet en Utilisateur
	 * @param ResultSet resultSet : données de retour de la requête en BDD
	 * @return Utilisateur : objet entity contenant les données
	 * constantes définies dans la classe com.greta.s2e_servlet.globals.Constantes
	 */
	private UserBean map(ResultSet resultSet) throws SQLException{
		UserBean element = new UserBean(resultSet.getInt(USR_ID),
				  resultSet.getString(USR_NOM), 
				  resultSet.getString(USR_PRENOM),
				  resultSet.getString(USR_LOGIN),
				  resultSet.getString(USR_PASSWORD),
				  resultSet.getString(USR_ID_ROLE), 
				  resultSet.getInt(USR_ID_ETB));
		return element;
	}

}
