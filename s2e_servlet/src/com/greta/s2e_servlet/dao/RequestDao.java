package com.greta.s2e_servlet.dao;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.greta.s2e_servlet.beans.ArticleBean;
import com.greta.s2e_servlet.beans.DemandeBean;
import com.greta.s2e_servlet.beans.EtatBean;
import com.greta.s2e_servlet.beans.UserBean;

import java.sql.Connection;
import java.sql.Date;

public class RequestDao extends Dao{

	private static final Logger LOGGER = Logger.getLogger(RequestDao.class);

	private ArrayList<DemandeBean> demandeList;
	private DateTimeFormatter fmt;

	/**
	 * Récupération de la liste des demandes
	 */
	public ArrayList<DemandeBean> requestList(UserBean user) {
		LOGGER.info("ok requestList");
		demandeList = new ArrayList<DemandeBean>();
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();
		
		ArrayList<ArrayList> tempList = sqlGetData(user);
		sqlDeconnection();
		demandeList = formatData(tempList);
		return demandeList;
	}
	
	/**
	 * Création d'une demande
	 */
	public int create(DemandeBean demande){
		LOGGER.info("ok create");
		PreparedStatement preparedStatement = null;
		int resultSet = 0;
		
		fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();
		
		try {
			// Préparation de la requête
			if (null != connection) {
				preparedStatement = connection.prepareStatement("INSERT INTO "+ DEMANDE +" ("
						+ DMD_QTE +", "+ DMD_DELAI +", " + DMD_DATE +", "
						+ DMD_ID_USER +", "+ DMD_ID_ART +", "+ DMD_ID_ETAT 
						+") VALUES (?,?,?,?,?,?);");
				preparedStatement.setInt(1, demande.getQuantite());
				preparedStatement.setInt(2, demande.getDelai());
				preparedStatement.setString(3, fmt.print(demande.getDate()));
				preparedStatement.setInt(4, demande.getUser().getId_User());
				preparedStatement.setInt(5, demande.getArticle().getId_Article());
				preparedStatement.setString(6, demande.getEtat().getId_Etat());
			} else {
				LOGGER.fatal("Connexion null");
			}
			
			// Execution de la requête
			try {
				resultSet = preparedStatement.executeUpdate();
			} catch (SQLException exception) {
				LOGGER.fatal("Erreur requête : " + exception);
			}
			
		} catch (SQLException exception) {
			LOGGER.fatal("query error : " + exception);
		} finally {
			//fermeture de la connexion
			sqlDeconnection();
		}
		return resultSet;
	}
	

	/*
	 * Requires driver to be loaded
	 */
	protected ArrayList<ArrayList> sqlGetData(UserBean user) {
		PreparedStatement preparedStatement = null;
		ResultSet resultat = null;
		int indexL;
		ArrayList<ArrayList> rowResult = new ArrayList<ArrayList>();
			
			try {
				// Préparation de la requête
				if (null != connection) {
					preparedStatement = connection.prepareStatement("SELECT " + ART_NOM + ", " + DMD_QTE + ", " + DMD_DATE + ", "
							+ ART_PRIXM + ", " + ETT_LIB + " FROM " + DEMANDE 
							+ " INNER JOIN " + ETATS + " ON " + DMD_ID_ETAT + " = " + ETT_ID 
							+ " INNER JOIN " + ARTICLES + " ON " + DMD_ID_ART + " = "+ ART_ID
							+ " INNER JOIN " + USERS + " ON " + DMD_ID_USER + " = "+ USR_ID 
							+ " WHERE " + USR_ID_ETB + " = ?");
					preparedStatement.setInt(1, user.getId_Etab());
				} else {
					LOGGER.fatal("Connexion null");
				}
				
				// Execution de la requête
				try {
					resultat = preparedStatement.executeQuery();
				} catch (SQLException exception) {
					LOGGER.fatal("Erreur requête : " + exception);
				}
				LOGGER.info("requete " + resultat);
				
				/* Mise en forme des données */
				while (resultat.next()) {
					indexL = 1;
					ArrayList<String> temprow = new ArrayList<String>();
					while (true) {
						try {
							temprow.add(resultat.getString(indexL));
						} catch (SQLException e) {
							break;
						}
						++indexL;
					}
					rowResult.add(temprow);
				}
			} catch (SQLException exception) {
				LOGGER.fatal("query error : " + exception);
			}
			
		return rowResult;
	}

	//Mise en forme d'une liste(liste) en une liste de demande
	private ArrayList<DemandeBean> formatData(ArrayList<ArrayList> list) {
		demandeList = new ArrayList<DemandeBean>();
		fmt = DateTimeFormat.forPattern("yyyy-MM-dd");

		for (ArrayList<String> item : list) {
			LOGGER.info("Demande " + item.get(1) + "," + fmt.parseDateTime(item.get(2)));
			LOGGER.info("Etat " + item.get(4));
			LOGGER.info("Article " + item.get(0) + "," + item.get(3));

			demandeList.add(new DemandeBean(Integer.parseInt(item.get(1)), fmt.parseLocalDate(item.get(2)),
					new ArticleBean(item.get(0), Float.valueOf(item.get(3))), new EtatBean(item.get(4))));
		}
		return demandeList;
	}
}
