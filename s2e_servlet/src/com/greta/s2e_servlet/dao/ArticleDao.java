package com.greta.s2e_servlet.dao;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.greta.s2e_servlet.beans.ArticleBean;
import com.greta.s2e_servlet.beans.StockBean;

public class ArticleDao extends Dao{

	private static final Logger LOGGER = Logger.getLogger(ArticleDao.class);
	private ArrayList<ArticleBean> articleList;
	
	/**
	 * Récupération de la liste des articles
	 */
	public ArrayList<ArticleBean> articleList(){
		LOGGER.info("ok articleList");
		articleList = new ArrayList<ArticleBean>();
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();
		
		ArrayList<ArrayList> tempList = sqlGetData();
		// déconnexion à la BDD
		sqlDeconnection();
		articleList = formatData(tempList);
		return articleList;
	}

	
	/*
	 * Requires driver to be loaded
	 */
	protected ArrayList<ArrayList> sqlGetData() {
		PreparedStatement preparedStatement = null;
		
		ResultSet resultat = null;
		int indexL;
		ArrayList<ArrayList> rowResult = new ArrayList<ArrayList>();
			
			try {
				// Préparation de la requête
				if (null != connection) {
//					preparedStatement = connection.prepareStatement("SELECT * FROM " + ARTICLES);
					preparedStatement = connection.prepareStatement("SELECT " + ART_ID + ", " + ART_NOM + ", " + ART_MARQUE + ", "
							+ ART_MODELE + ", " + ART_EQUI + " FROM " + ARTICLES );
				} else {
					LOGGER.fatal("Connexion null");
				}

				// Execution de la requête
				try {
					resultat = preparedStatement.executeQuery();
				} catch (SQLException exception) {
					LOGGER.fatal("Erreur requête : " + exception);
				}
				LOGGER.info("requete " + resultat);
				
				/* Mise en forme des données */
				while (resultat.next()) {
					indexL = 1;
					ArrayList<String> temprow = new ArrayList<String>();
					while (true) {
						try {
							temprow.add(resultat.getString(indexL));
						} catch (SQLException e) {
							break;
						}
						++indexL;
					}
					rowResult.add(temprow);
				}
			} catch (SQLException exception) {
				LOGGER.fatal("query error : " + exception);
			}
			
		return rowResult;
	}
	
	/**
	 * Mise en forme de la liste(liste) en une liste d'Articles
	 */
	private ArrayList<ArticleBean> formatData(ArrayList<ArrayList> list) {
		articleList = new ArrayList<ArticleBean>();

		for (ArrayList<String> item : list) {
			LOGGER.info("Article " + item.get(1) + "," + item.get(2));

			/**
			 * @param id_Article
			 * @param nom
			 * @param marque
			 * @param modele
			 * @param equi
			 */
			articleList.add(new ArticleBean(Integer.parseInt(item.get(0)), item.get(1),
					item.get(2), item.get(3), Integer.parseInt(item.get(4))));
		}
		return articleList;
	}
	
	/**
	 * Récupération de la quantité du stock pour un article
	 */
	public int getStockById(int idArticle){
		LOGGER.info("ok getStockById");
		PreparedStatement preparedStatement = null;
		ResultSet resultat = null;
		int result = 0;
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();
		
		try {
			// Préparation de la requête
			if (null != connection) {
				preparedStatement = connection.prepareStatement("SELECT " + STK_QUANT +" FROM " + STOCK
						+ " WHERE " + STK_ID_ART + "=?");
				preparedStatement.setInt(1, idArticle);
			} else {
				LOGGER.fatal("Connexion null");
			}

			// Execution de la requête
			try {
				resultat = preparedStatement.executeQuery();
			} catch (SQLException exception) {
				LOGGER.fatal("Erreur requête : " + exception);
			}
			LOGGER.info("requete " + resultat);
			
			//Récupération de la valeur
			while (resultat.next()) {
				result = Integer.parseInt(resultat.getString(1));
				LOGGER.info("requete en forme " + result);
			}
			
		} catch (SQLException exception) {
			LOGGER.fatal("query error : " + exception);
		} finally {
			// déconnexion à la BDD
			sqlDeconnection();
		}

		return result;
	}
	
	/**
	 * Récupération de la liste des articles en fonction de l'équivalence
	 */
	public ArrayList<ArticleBean> articleListByEqui(int idArticle){
		LOGGER.info("ok articleList");
		articleList = new ArrayList<ArticleBean>();
		
		// chargement du driver
		sqlLoadDriver();
		// connexion à la BDD
		sqlConnection();
		
		ArrayList<ArrayList> tempList = sqlGetDataByEqui(idArticle);
		// déconnexion à la BDD
		sqlDeconnection();
		articleList = formatData(tempList);
		return articleList;
	}
	
	/*
	 * récupération liste par équivalence
	 */
	protected ArrayList<ArrayList> sqlGetDataByEqui(int idArticle) {
		PreparedStatement preparedStatement = null;
		
		ResultSet resultat = null;
		int indexL;
		ArrayList<ArrayList> rowResult = new ArrayList<ArrayList>();
			
			try {
				// Préparation de la requête
				if (null != connection) {
					preparedStatement = connection.prepareStatement("SELECT " + ART_ID + ", " + ART_NOM + ", " + ART_MARQUE + ", "
							+ ART_MODELE + ", " + ART_EQUI + " FROM " + ARTICLES 
							+ " WHERE " + ART_EQUI + "= (SELECT " + ART_EQUI + " FROM " + ARTICLES
							+ " WHERE " + ART_ID + "= ?)");
					preparedStatement.setInt(1, idArticle);
				} else {
					LOGGER.fatal("Connexion null");
				}

				// Execution de la requête
				try {
					resultat = preparedStatement.executeQuery();
				} catch (SQLException exception) {
					LOGGER.fatal("Erreur requête : " + exception);
				}
				LOGGER.info("requete " + resultat);
				
				/* Mise en forme des données */
				while (resultat.next()) {
					indexL = 1;
					ArrayList<String> temprow = new ArrayList<String>();
					while (true) {
						try {
							temprow.add(resultat.getString(indexL));
						} catch (SQLException e) {
							break;
						}
						++indexL;
					}
					rowResult.add(temprow);
				}
			} catch (SQLException exception) {
				LOGGER.fatal("query error : " + exception);
			}
			
		return rowResult;
	}
}
