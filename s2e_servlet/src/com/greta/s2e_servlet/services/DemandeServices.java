package com.greta.s2e_servlet.services;

import static com.greta.s2e_servlet.globals.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import com.greta.s2e_servlet.beans.ArticleBean;
import com.greta.s2e_servlet.beans.DemandeBean;
import com.greta.s2e_servlet.beans.EtatBean;
import com.greta.s2e_servlet.beans.UserBean;
import com.greta.s2e_servlet.dao.ArticleDao;
import com.greta.s2e_servlet.dao.RequestDao;
import com.greta.s2e_servlet.dao.UserDao;

public class DemandeServices {

	private RequestDao requestDao;
	private UserDao userDao;
	private ArticleDao articleDao;
	private ArrayList<DemandeBean> demandeList;
	private ArrayList<ArticleBean> articleList;
	private static final Logger LOGGER = Logger.getLogger(DemandeServices.class);
	private DemandeBean demande;
	
	/**
	 * ATTRIBUTES
	 */
	protected String result;
	protected Map<String,String> errors = new HashMap<String,String>();

	/**
	 * Initialise les différentes Dao utilisées
	 */
	public DemandeServices() {
		requestDao = new RequestDao();
		userDao = new UserDao();
		articleDao = new ArticleDao();
		demandeList = new ArrayList<DemandeBean>();
		articleList = new ArrayList<ArticleBean>();
	}
	

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @return the errors
	 */
	public Map<String, String> getErrors() {
		return errors;
	}

	/**
	 * @return une liste de demande concernant un utilisateur qui appartient
	 * à un établissement
	 */
	public ArrayList<DemandeBean> requestList(UserBean user) {
		LOGGER.info("requestList avec idUser = " + user.getId_User());
		LOGGER.info("et voici l'id de l'établissement " + user.getId_Etab());
		
		if (user.getId_Etab() == 0) {
			demandeList = null;
		} else {
			demandeList = requestDao.requestList(user);
		}	
		return demandeList;
	}
	
	/**
	 * Vérification du stock pour la nouvelle demande d'article
	 * en fonction de l'article et de la quantité
	 */
	public ArrayList<ArticleBean> verifyStockDemande(HttpServletRequest request){
		String id_Article = valueFromParam(request , ARTICLE_PARAM);
		LOGGER.info("retour formulaire avec id = " + id_Article);
		
		String qte_Article = valueFromParam(request , QUANT_PARAM);
		LOGGER.info("retour formulaire avec quantité = " + qte_Article);
		
		articleList = null;
		verifyDemande (id_Article, qte_Article);
		if ( errors.isEmpty() ) {
			int stock = articleDao.getStockById(Integer.parseInt(id_Article));
			if (stock < Integer.parseInt(qte_Article)){
				articleList = articleDao.articleListByEqui(Integer.parseInt(id_Article));
			}
		}
		return articleList;
	}
	
	
	/**
	 * Création d'une nouvelle demande d'article en fonction d'un utilisateur
	 */
	public void createDemande(HttpServletRequest request, UserBean user){
		String id_Article = valueFromParam(request , ARTICLE_PARAM);
		LOGGER.info("retour formulaire avec id = " + id_Article);
		
		String id_ArtiEqui = valueFromParam(request , ARTICLEEQUI_PARAM);
		LOGGER.info("retour formulaire avec id = " + id_ArtiEqui);
		
		String qte_Article = valueFromParam(request , QUANT_PARAM);
		LOGGER.info("retour formulaire avec quantité = " + qte_Article);
		
		if (id_ArtiEqui == null) {
			verifyDemande (id_Article, qte_Article);
		}else {
			if (id_ArtiEqui == id_Article) {
				verifyDemande (id_Article, qte_Article);
			} else {
				id_Article = id_ArtiEqui;
				verifyDemande (id_Article, qte_Article);
			}
		}
		
		LocalDate dateJ= LocalDate.now();
		demande = new DemandeBean();

		if ( errors.isEmpty() ) {
	        result = SUCCESS_MESSAGE;
			demande = new DemandeBean(Integer.parseInt(qte_Article), DELAI_DEMANDE, dateJ, 
			new UserBean(user.getId_User()), 
			new ArticleBean(Integer.parseInt(id_Article)), 
			new EtatBean(VALI_ETAT, null));
			requestDao.create(demande);
	    } else {
	        result = FAILURE_MESSAGE;
	    }
	}
	
	/**
	 * Appelle la méthode validateArticle pour chaque champ
	 */
	private void verifyDemande (String id_Article, String qte_Article){

		try {
			validateArticle(id_Article);
		} catch (Exception e) {
			setError ( ARTICLE_PARAM, e.getMessage());
		}
		try {
			validateArticle(qte_Article);
		} catch (Exception e) {
			setError ( QUANT_PARAM, e.getMessage());
		}
	}

//-----------------------------------------------------------------------------------------------------------
	/**
	 * Vérifie si les champs de la request sont non null et ont une longueur
	 */
	private static String valueFromParam (HttpServletRequest request, String name){
		String value = request.getParameter(name);
		if ( (null == value) || (0 == value.trim().length()) ){
			value = null;
		} else {
			value = value.trim();
		}
		return value;
	}
	
	/**
	 * Vérifie si un champ du formulaire Article (id et quantité) soit bien un chiffre
	 * sans être égal à 0,
	 * renvoit une erreur si vrai
	 */
	private void validateArticle(String Article) throws Exception{
		String pattern = "^[0-9]+$";
		LOGGER.info("regex " + (0 == Integer.parseInt(Article)) + " et " + (Article.matches(pattern) == false));
		if ( (0 == Integer.parseInt(Article))  || (Article.matches(pattern) == false) ) {
	        throw new Exception( ARTICLE_VALIDATION_ERROR );
	    }
		
	}

	/**
	 * Insère les messages d'erreur dans une map avec la cause de l'erreur et le message
	 */
	private int setError (String errorCause, String errorMessage) {
		int status= -1;
		status = (errorMessage == errors.put(errorCause, errorMessage))?1:0;
		return status;
	}
}
