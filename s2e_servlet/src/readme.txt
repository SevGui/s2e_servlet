Nom du projet : s2e_servlet
Techno : httpservlet + JSP(scriplet : story.jsp, JSTL : new.jsp)/JDBC
Pour qu'il fonctionne chez vous, il faut mettre vos données personnelles dans :
- la classe com.greta.s2e_servlet.globals.Constants

! Ne pas oublier de mettre le driver JDBC : mysql-connector-java-5.1.40-bin.jar dans le dossier lib de Tomcat (C:\Program Files\Apache Software Foundation\Tomcat 8.0\lib)

Bibliothèques à mettre dans /WEB-INF/lib/ :
jstl-1.2.jar
log4j-1.2.17.jar
joda-time-2.9.4.jar