<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="static com.greta.s2e_servlet.globals.Constants.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<select id="<%=ARTICLEEQUI_PARAM%>" name="<%=ARTICLEEQUI_PARAM%>" class="form-control">
	<c:forEach items="${articleequi}" var="item">
		<option value="${item.id_Article}">${item.nom} ${item.marque} ${item.modele}</option>
	</c:forEach>
</select>
<span id="errorMsgEqui" class="span"></span>