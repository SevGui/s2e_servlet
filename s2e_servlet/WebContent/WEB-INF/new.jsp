<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="static com.greta.s2e_servlet.globals.Constants.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/CSS" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css"/>
	<title>S2E - Etablissement</title>
</head>
<body>
	<div class="container-fluid">
		<header class="row">
			<%@ include file="/template/header.jsp"%>
		</header>
		
		<nav class="mainmenu">
			<div class="container">
				<%@ include file="/template/links.jsp"%>
			</div>
		</nav>
		<main class="container">
			<div class="row">
				<div class="box col-xs-offset-3 col-xs-5 text-center">
					<div class="panel">
						<div class="title">
							<h1>Budget restant pour l'établissement :</h1>
						</div>
					</div>
					<div class="content">
						<c:out value="${budgetrest}"></c:out>
						Euros
					</div>
				</div>
			</div>
			<div class="row">
				<form method="post" action="new" class="form form-horizontal col-xs-offset-2 col-xs-8" id="myForm">
				<fieldset>
		
					<!-- Form Name -->
					<legend>Demande d'article</legend>
		
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="<%=ARTICLE_PARAM%>">Article :</label>
						<div class="col-md-4">
							<select id="<%=ARTICLE_PARAM%>" name="<%=ARTICLE_PARAM%>" class="form-control" onchange="testquantite();">
								<c:forEach items="${article}" var="item">
									<option value="${item.id_Article}">${item.nom} ${item.marque} ${item.modele}</option>
								</c:forEach>
							</select>
							<span id="errorMsgArticle" class="span"></span>
						</div>
					</div>
		
		 			<!-- Select Basic -->
					<div class="form-group" id="articleequidiv" style="display: none;"> 
						<label class="col-md-4 control-label" for="<%=ARTICLEEQUI_PARAM%>">Article équivalent :</label>
						<div class="col-md-4" id="artequiselect">
							
		 				</div>
		 			</div>
		
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="<%=QUANT_PARAM%>">Quantité :</label>
						<div class="col-md-4">
							<input id="<%=QUANT_PARAM%>" name="<%=QUANT_PARAM%>"
								class="form-control input-md" required="" type="text" onchange="testquantite();"/>
							<span id="errorMsgQuantite" class="span"></span>
						</div>
					</div>
					
					<span>${erreur.errors['article']}</span>
					<span>${erreur.errors['quantite']}</span>
					
					<!-- Button -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="valide"></label>
						<div class="col-md-4">
							<button id="valide" name="valide" class="btn">Valider</button>
						</div>
					</div>
					
				</fieldset>
			</form>
			</div>
		</main>
		<footer class="container footer-wrap"> 
			<%@ include file="/template/footer.jsp"%>
		</footer>
	
</div>
<script src="<%=request.getContextPath()%>/js/script.js"></script>

</body>
</html>