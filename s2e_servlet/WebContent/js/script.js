/**
 * 
 */
// "use strict";
function testquantite() {
	// alert("coucou");
	// récupération des valeurs du formulaire
	var myForm = document.getElementById("myForm");
	var errorMsgArt = document.getElementById("errorMsgArticle");
	var errorMsgQut = document.getElementById("errorMsgQuantite");

	// alert(myForm.article.value);

	// condition: chiffre de 0 à 9 d'une longueur minimum de 1
	// myForm.article.value récupère la valeur de article dans le jsp
	// myForm.quantite.value récupère la valeur de quantite dans le jsp
	if (/^[0-9]+$/.test(myForm.article.value) || Number(myForm.article.value) != 0) {
		// alert("OK");
		errorMsgArt.innerHTML = "";
		if (/^[0-9]+$/.test(myForm.quantite.value) || Number(myForm.quantite.value) != 0) {
			// alert("OK");
			errorMsgQut.innerHTML = "";
			// appelle la requête ajax
			test_stock_ajax_request(myForm.article.value, myForm.quantite.value);
		} else {
			// alert("pas OK");
			// sinon renvoi un message d'erreur
			errorMsgQut.innerHTML = "Veuillez mettre une quantité >0";
		}
	} else {
		// alert("pas OK");
		// sinon renvoi un message d'erreur
		errorMsgArt.innerHTML = "Veuillez sélectionner un article dans la liste";
	}
}

function test_stock_ajax_request(article, quantite) {
	var select = document.getElementById("artequiselect");
	var div = document.getElementById("articleequidiv");
	
	// on instancie l'objet XMLHttpRequest
	var xhr = new XMLHttpRequest();

	//alert("yo");

	// je redirige vers la servlet ajax
	xhr.open("GET", "http://localhost:8080/s2e_servlet/ajax?article=" + article + "&quantite=" + quantite, true);
	//alert("yo");
	
	//Au retour de la servlet, on récupère le ajax.jsp si l'état et le status sont correct
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4 && xhr.status === 200) {
			
			//récupération
			 var artEqui = xhr.responseText;
			 
			 //insertion dans le new.jsp
			 select.innerHTML = artEqui;
			 
			 //vérification qu'il y a au moins une valeur dans le menu déroulant
			 var articleEqui = document.getElementById("articleequi");
			 var errorMsgeQui = document.getElementById("errorMsgEqui");
			 
			 //alert(errorMsgeQui.value);
			 errorMsgeQui.innerHTML = "La quantité est trop importante, veuillez sélectionner " +
			 		"soit un équivalent, soit l'article épuisé pour une commande";
			 
			 //alert(articleEqui.value);
			 if (Number(articleEqui.value) != 0){
				 
				 //si oui on affiche la div
				 div.style.display = "block";
				 
			 }
		}
	};
	xhr.send();
}
