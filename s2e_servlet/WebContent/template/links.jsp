<%@ page import="static com.greta.s2e_servlet.globals.Constants.*"%>
<ul class="nav nav-pills col-sm-offset-0 col-sm-5">
	<li class="active"><a href="<%=request.getContextPath() + PAGE_HISTORIQUE%>"><%=MENU_HISTORIQUE%></a></li>
	<li><a href="<%=request.getContextPath() + PAGE_DEMANDE%>"><%=MENU_DEMANDE%></a></li>
	<li><a href="<%=request.getContextPath() + PAGE_DECONNEXION%>"><%=MENU_DECONNEXION%></a></li>
</ul>
<ul class="nav nav-pills col-sm-offset-3 col-sm-4">
	<li><a class="social" href="#"><img src="images/aim.png"
			alt="" /></a></li>
	<li><a class="social" href="#"><img src="images/facebook.png"
			alt="" /></a></li>
	<li><a href="#"><img src="images/twwtter.png" alt="" /></a></li>
	<li><a href="#"><img src="images/linkedin.png" alt="" /></a></li>
</ul>