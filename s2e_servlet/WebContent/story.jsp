<%@page import="com.greta.s2e_servlet.beans.DemandeBean"%>
<%@page import="org.joda.time.format.DateTimeFormat"%>
<%@page import="org.joda.time.format.DateTimeFormatter"%>
<%@ page import="static com.greta.s2e_servlet.globals.Constants.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%
	ArrayList<DemandeBean> listeDemandes = (ArrayList<DemandeBean>) request.getAttribute(DEMANDE_ATTR);
	DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/CSS" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<title>S2E - Etablissement</title>
</head>
<body>
	<div class="container-fluid">
		<header class="row">
			<%@ include file="/template/header.jsp"%>
		</header>
		
		<nav class="mainmenu">
			<div class="container">
				<%@ include file="/template/links.jsp"%>
			</div>
		</nav>
		
		<main class="container">
			<div class="row">
				<div class="box col-xs-offset-1 col-xs-4 text-center">
					<div class="panel">
						<div class="title">
							<h1>Budget initial pour l'établissement :</h1>
						</div>
					</div>
					<div class="content">
						<%=request.getAttribute(BUDGET_INT_ATTR)%>
						Euros
					</div>
				</div>
				<div class="box col-xs-offset-2 col-xs-5 text-center">
					<div class="panel">
						<div class="title">
							<h1>Budget restant pour l'établissement :</h1>
						</div>
					</div>
					<div class="content">
						<%=request.getAttribute(BUDGET_RST_ATTR)%>
						Euros
					</div>
				</div>
			</div>
			<div class="row">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Article</th>
							<th>Quantité</th>
							<th>Montant Unitaire</th>
							<th>Date</th>
							<th>Montant</th>
							<th>Etat</th>
						</tr>
					</thead>
					<tbody>
						<%
							if (listeDemandes != null) {
								for (DemandeBean demande : listeDemandes) {
						%>
						<tr>
							<td><%=demande.getArticle().getNom()%></td>
							<td><%=demande.getQuantite()%></td>
							<td><%=demande.getArticle().getPrix()%></td>
							<td><%=demande.getDate().toString(fmt)%></td>
							<td><%=demande.getQuantite() * demande.getArticle().getPrix()%></td>
							<td><%=demande.getEtat().getLibelle()%></td>
						</tr>
						<%
							}
							}
						%>
					</tbody>
				</table>
			</div>
		</main>
		<footer class="container footer-wrap"> 
			<%@ include file="/template/footer.jsp"%>
		</footer>
	</div>
</body>
</html>